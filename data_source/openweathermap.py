"""
Open weather map data source
https://openweathermap.org/
"""

import requests
import settings
from datetime import datetime


def get_points(postcode):
    print("Retrieving weather information for: ", postcode)
    weather_query = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?zip={postcode},us&appid={settings.OPENWEATHERMAP_KEY}&units=imperial"
    )
    weather_query.raise_for_status()
    weather = weather_query.json()

    sunrise = datetime.utcfromtimestamp(float(weather["sys"]["sunrise"]))
    sunset = datetime.utcfromtimestamp(float(weather["sys"]["sunset"]))
    total_sun = sunset - sunrise

    return [
        {
            "measurement": "weather",
            "tags": {
                "location": weather["name"], 
                "postcode": postcode
                # "main": weather["weather"]["main"]
                # "description": weather["weather"]["description"]
            },
            "time": weather["dt"] * 1000000000,
            "fields": {
                "temp": float(weather["main"]["temp"]),
                "feels_like": float(weather["main"]["feels_like"]),
                "temp_min": float(weather["main"]["temp_min"]),
                "temp_max": float(weather["main"]["temp_max"]),
                "pressure": float(weather["main"]["pressure"]),
                "humidity": float(weather["main"]["humidity"]),
                "wind_speed": float(weather["wind"]["speed"]),
                "wind_direction": float(weather["wind"]["deg"]),
                "clouds": float(weather["clouds"]["all"]),
                "sunlight": round(total_sun.total_seconds() / 60 / 60, 2)
            },
        }
    ]
